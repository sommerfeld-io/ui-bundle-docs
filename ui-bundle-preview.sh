#!/bin/bash
# @file ui-bundle-preview.sh
# @brief Run the preview for this UI bundle.
#
# @description The script runs all linters and runs a local webserver on port 5252. Content is the preview of this ui
# bundle. Updates to the ui bundle files are loaded automatically (so there is no need to restart the server every time).
#
# ==== Arguments
#
# The script does not accept any parameters.

cat << EOF

    _____   ___    _____   ___
   | ____| |__ \  | ____| |__ \

   | |__      ) | | |__      ) |
   |___ \    / /  |___ \    / /
    ___) |  / /_   ___) |  / /_
   |____/  |____| |____/  |____|


EOF

(
  cd src/main/ui-bundle || exit

  echo -e "$LOG_INFO Preview ui-bundle in local webserver"
  gulp preview
)
