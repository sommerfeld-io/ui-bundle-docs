#!/bin/bash
# @file ui-bundle-run-template.sh
# @brief Run the template in a local webserver.
#
# @description The script runs the template in a local webserver on port 9192. The template files are the assets of the static html template which is the foundation for the ui bundle.
#
# ==== Arguments
#
# The script does not accept any parameters.

cat << EOF

     ___    __    ___    ___
    / _ \  /_ |  / _ \  |__ \

   | (_) |  | | | (_) |    ) |
    \__, |  | |  \__, |   / /
      / /   | |    / /   / /_
     /_/    |_|   /_/   |____|


EOF

echo -e "$LOG_INFO Running template in local webserver"
#webserver 9192 src/main/templates/dasho-bootstrap-admin-template/Template/dist
#webserver 9192 src/main/templates/falcon-v3.5.1-and-v2.8.2/falcon-v2.8.2/pages

echo -e "LOG_INFO Custom docs page =$Y http://localhost:9192/_docs-pages/docs-page-proxmox.html$D"
webserver 9192 src/main/templates/falcon-v3.5.1-and-v2.8.2/falcon-v3.5.1/public
